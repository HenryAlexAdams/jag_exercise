// this is the Javascript version of the Jaguar Design Studio Developer Exercise

function Marklar(string) {
    // first turn the string into an array of words
    // then work through the array, testing the length of each word
    var aWords = new Array();
    var word = '';
    var firstLetter = '';

    aWords = string.split(" ");

    for (word in aWords) {
        if (aWords[word].length > 4) {
            firstLetter = aWords[word].substring(0,1);
            if (firstLetter == firstLetter.toUpperCase()) {
                // it's an uppercase letter, so replace the word with Marklar
                aWords[word] = 'Marklar';
            } else {
                // it's a lowercase letter, so replace the word with marklar
                aWords[word] = 'marklar';
            }
        }
    }

    var newString = aWords.toString();
    var returnString = newString.replace(/,/gi, " ");

    return returnString;
};

function Fibonacci(nth) {
    // the nth parameter is the number terms in the Fibonacci sequence
    // return zero if there is no parameter, if the parameter is not an integer (or cannot be converted to an integer), or if nth is less than two
    if (!nth) {
        return 0;
    }

    var cleanNth = parseInt(nth);

    if (cleanNth <= 2) {
        return 0;
    }

    // since we are still in the game, prepare variables
    var termsArray = new Array();
    var currentTermPlace = 1;
    var currentTermValue = 0;
    var evenNumbersSum = 0;
    var evenNumberValue = 0;

    // if we are at this point, we have at least 3 terms, so start there
    termsArray = [
        {
            "termPlace": 1,
            "termValue": 1,
            "evenValue": 0
        },
        {
            "termPlace": 2,
            "termValue": 1,
            "evenValue": 0
        }
    ];

    currentTermPlace = 3;
    while (currentTermPlace <= cleanNth) {
        currentTermValue = termsArray[currentTermPlace - 3].termValue + termsArray[currentTermPlace - 2].termValue;
        if (currentTermValue%2 == 0) {
            // it's even
            evenNumberValue = currentTermValue;
            evenNumbersSum = evenNumbersSum + currentTermValue;
        } else {
            evenNumberValue = 0;
        }

        var newTerm = {
            "termPlace": currentTermPlace,
            "termValue": currentTermValue,
            "evenValue": evenNumberValue
        }

        termsArray.push(newTerm);

        currentTermPlace++;
    }

    return evenNumbersSum;

};