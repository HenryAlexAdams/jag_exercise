<?php
// these functions are used by Jaguar Design Studio's Developer Exercise

function Marklar($string) {
	// first turn the string into an array of words
	// then work through the array, testing the length of each word
	$aWords = array();
	$word = '';
	
	$aWords = explode(" ", $string);
	$firstLetter = '';
	foreach ($aWords as &$word) {
		if (strlen($word) > 4) {
			$firstLetter = substr($word,0,1);
			if ($firstLetter == strtoupper($firstLetter)) {
				// it's an uppercase letter, so replace the word with Marklar
				$word = 'Marklar';
			} else {
				// it's a lowercase letter, so replace the word with marklar
				$word = 'marklar';
			}
		}
	}

	$newString = implode(" ", $aWords);

	return $newString;
}

function Fibonacci($nth) {
	// the nth parameter is the number terms in the Fibonacci sequence
	// return zero if there is no parameter, if the parameter is not an integer (or cannot be converted to an integer), or if nth is less than two
	if (!$nth) {
		return 0;
	}
	$cleanNth = intval($nth);
	if (!is_int($cleanNth)) {
		return 0;
	}
	if ($cleanNth <= 2) {
		return 0;
	}

	// since we are still in the game, prepare variables
	$termsArray = array();
	$currentTermPlace = 1;
	$currentTermValue = 0;
	$evenNumbersSum = 0;

	// if we are at this point, we have at least 3 terms, so start there
	$term1 = array(
		"termPlace" => 1,
		"termValue" => 1,
		"evenValue" => 0
	);
	$term2 = array(
		"termPlace" => 2,
		"termValue" => 1,
		"evenValue" => 0
	);
	
	$termsArray[] = $term1;
	$termsArray[] = $term2;
	
	$currentTermPlace = 3;
	
	while ($currentTermPlace <= $cleanNth) {
		$twoBackPlace = $currentTermPlace - 3;
		$twoBackValue = $termsArray[$twoBackPlace]['termValue'];
		$oneBackPlace = $currentTermPlace - 2;
		$oneBackValue = $termsArray[$oneBackPlace]['termValue'];
	
		$currentTermValue = $twoBackValue + $oneBackValue;
		
		if ($currentTermValue%2 == 0) {
			// it's even
			$evenNumberValue = $currentTermValue;
			$evenNumbersSum = $evenNumbersSum + $currentTermValue;
		} else {
			$evenNumberValue = 0;
		}
		
		$newTerm = array(
			"termPlace" => $currentTermPlace,
			"termValue" => $currentTermValue,
			"evenValue" => $evenNumberValue
		);

		$termsArray[] = $newTerm;
		
		$currentTermPlace++;
	}
	
	return $evenNumbersSum;
}