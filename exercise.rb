class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # TODO: Implement this method

    # first turn the string into an array of words
    # then work through the array, testing the length of each word
    aWords = Array.new
    word = ''
    firstLetter = ''

    # unlike Javascript and PHP, ruby drops the period and question mark during the split
    # ruby is new for me, so I included this hack to accomplish this exercise
    # I assume there is a more elegant way to handle this

    tempStrPPP = str.gsub(".", " PPP")
    tempStrQQQ = tempStrPPP.gsub("?", " QQQ")
    tempStrXXX = tempStrQQQ.gsub("!", " XXX")
    tempStrCCC = tempStrXXX.gsub(",", " CCC")
    tempStrDDD = tempStrCCC.gsub('"', " DDD")
    tempStrSSS = tempStrDDD.gsub("'", " SSS")

    aWords = tempStrSSS.split(" ")

    i = 0

    aWords.each do |word|
      if word.length > 4
        firstLetter = word[0,1]

        if firstLetter == firstLetter.upcase
          word = 'Marklar'
        else
          word = 'marklar'
        end #if uppercase
        aWords[i] = word
      end #if word.length > 4
      i += 1
    end #each

    newStringStart = aWords.join(" ")

    # restoring dropped punctuation

    newStringPPP = newStringStart.gsub(" PPP", ".")
    newStringQQQ = newStringPPP.gsub(" QQQ", "?")
    newStringXXX = newStringQQQ.gsub(" XXX", "!")
    newStringCCC = newStringXXX.gsub(" CCC", ",")
    newStringDDD = newStringCCC.gsub(" DDD", '"')
    newStringSSS = newStringDDD.gsub(" SSS", "'")
    newStringFinal = newStringSSS

    return newStringFinal

  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # TODO: Implement this method
    # the nth parameter is the number terms in the Fibonacci sequence
    # return zero if there is no parameter, if the parameter is not an integer (or cannot be converted to an integer), or if nth is less than two

    if !nth
      return 0
    end #if

    cleanNth = Integer(nth)

    if cleanNth <=2
      return 0
    end #if

    # since we are still in the game, prepare variables
    termsArray = Array.new
    currentTermPlace = 1
    currentTermValue = 0
    evenNumbersSum = 0
    evenNumberValue = 0
    twoBackPlace = 0
    twoBackValue = 0
    oneBackPlace = 0
    oneBackValue = 0

    # if we are at this point, we have at least 3 terms, so start there

    termsArray[currentTermPlace -1] = Hash.new
    termsArray[currentTermPlace -1][:termPlace] = 1
    termsArray[currentTermPlace -1][:termValue] = 1
    termsArray[currentTermPlace -1][:evenValue] = 0

    currentTermPlace = 2

    termsArray[currentTermPlace -1] = Hash.new
    termsArray[currentTermPlace -1][:termPlace] = 2
    termsArray[currentTermPlace -1][:termValue] = 1
    termsArray[currentTermPlace -1][:evenValue] = 0

    currentTermPlace = 3

    while currentTermPlace <= cleanNth do
      twoBackPlace = currentTermPlace - 3
      twoBackValue = termsArray[currentTermPlace - 3][:termValue]
      oneBackPlace = currentTermPlace - 2
      oneBackValue = termsArray[currentTermPlace - 2][:termValue]

      currentTermValue = twoBackValue + oneBackValue

      if currentTermValue%2 == 0
        # it's even
        evenNumberValue = currentTermValue
        evenNumbersSum = evenNumbersSum + currentTermValue
      else
        evenNumberValue = 0
      end #if

      termsArray[currentTermPlace - 1] = Hash.new
      termsArray[currentTermPlace - 1][:termPlace] = currentTermPlace
      termsArray[currentTermPlace -1][:termValue] = currentTermValue
      termsArray[currentTermPlace - 1][:evenValue] = evenNumberValue

      currentTermPlace = currentTermPlace + 1

    end #while

    return evenNumbersSum

  end #def

end #class
