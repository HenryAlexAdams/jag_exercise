## Jaguar Design Studio
## Web Application Developer Exercise

### Exercise Notes - Alex Adams, alex@a2technology.com, http://www.linkedin.com/in/alexadamsa2/, 831-331-3895

I am new to Ruby.  Previously, I had done a couple of short tutorials, but I hadn’t done any independent development.  Consequently, I did this exercise first in two languages that I am more familiar with, Javascript and PHP.  I successfully implemented the Javascript functions in a Meteor.js app that I am working on, and the PHP functions in a Yii framework app.  The process was relatively easy for both.  I discovered that Javascript and PHP are more similar to each other than either is to Ruby.

The Ruby exercise.rb passes the tests in exercise_spec.rb.